# Generated by Django 2.1.7 on 2019-06-06 07:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('unoapp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofileinfo',
            name='profile_pic',
        ),
    ]
