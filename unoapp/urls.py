from django.urls import path
from django.conf.urls import url
from unoapp import views
# app_name = 'app1'

urlpatterns = [
path('register/',views.register,name='register'),
path('user_login/',views.user_login,name='user_login'),
path('logout/',views.user_logout,name='logout'),
path('change_password/', views.change_password, name='change_password'),
url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
]
